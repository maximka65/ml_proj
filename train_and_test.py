import data_processing as dp
import model
import pickle

def train_and_evaluate():
    X, y = dp.load_data()
    X_train, X_test, y_train, y_test = dp.split_data(X, y)

    regression_model = model.create_model()
    regression_model.fit(X_train, y_train)

    train_score = regression_model.score(X_train, y_train)
    test_score = regression_model.score(X_test, y_test)

    print(f"Train Score: {train_score}")
    print(f"Test Score: {test_score}")

    return regression_model


if __name__ == "__main__":
    model = train_and_evaluate()
    with open('regression_model.pkl', 'wb') as f:
        pickle.dump(model, f)