import mlflow
import mlflow.sklearn
from sklearn.ensemble import RandomForestRegressor
from sklearn.datasets import load_boston
from sklearn.model_selection import train_test_split

def run_mlflow_experiment():
    # Начните запуск MLflow
    mlflow.start_run()

    # Загрузите и разделите данные
    data = load_boston()
    X = data.data
    y = data.target
    X_train, X_test, y_train, y_test = train_test_split(X, y)

    # Обучите модель
    model = RandomForestRegressor()
    model.fit(X_train, y_train)

    # Логирование параметров
    mlflow.log_param("n_estimators", model.n_estimators)
    mlflow.log_param("max_depth", model.max_depth)

    # Логирование метрик
    test_score = model.score(X_test, y_test)
    mlflow.log_metric("test_score", test_score)

    # Логирование модели
    mlflow.sklearn.log_model(model, "random_forest_regressor_model")

    # Завершите запуск MLflow
    mlflow.end_run()

if __name__ == "__main__":
    run_mlflow_experiment()
