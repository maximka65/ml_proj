import click
from src import mlflow_training

@click.group()
def main():
    pass

@main.command()
def mlflow_experiment():
    """Run MLflow experiment with RandomForestRegressor on boston dataset."""
    mlflow_training.run_mlflow_experiment()

# Добавьте другие команды, если они есть

if __name__ == "__main__":
    main()
